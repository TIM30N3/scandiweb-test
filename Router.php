<?php

namespace app;

class Router{
    public array $getRoutes = [];
    public array $postRoutes = [];

    public function get($url, $fn)
    {
        $this->getRoutes[$url] = $fn;
    }

    public function post($url, $fn)
    {
        $this->postRoutes[$url] = $fn;
    }

    public function resolve()
    {
        $method = strtolower($_SERVER['REQUEST_METHOD']);
        $url = $_SERVER['REQUEST_URI'];

        if(strpos($url, '?') !== false){
            $url = substr($url, 0, strpos($url, '?'));
        }

        if($method === 'get'){
            $fn = $this->getRoutes[$url] ?? null;
        } else {
            $fn = $this->postRoutes[$url] ?? null;
        }

        if(!$fn){
            echo '<h1>Page not found</h1>';
            exit;
        }

        echo call_user_func($fn, $this);
    }

    public function renderView($view)
    {
        ob_start();
        include_once __DIR__."/view/partials/$view.php";
        $content = ob_get_clean();
        include_once __DIR__."/view/layout.php";
    }
}