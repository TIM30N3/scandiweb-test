<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/css/mutual.css">
    <link rel="stylesheet" href="style/css/productList.css">
    <link rel="stylesheet" href="style/css/addProduct.css">
    <title>JDProject</title>
</head>
<body>
    <?php echo $content;?>
    <footer>
        <p>Scandiweb Test assigment</p>
    </footer>
</body>
</html>