<script src="js/productInputs.js" defer></script>
<script src="js/invalidValues.js" defer></script>

<header>
    <div>
        <h1 class="pageTitle" >Product Add</h1>

        <div>
            <form>
                <input type="button" id="save" value="SAVE" form="addProduct">
            </form>
            <form method="post" action="/">
                <input type="submit" value="CANCEL">
            </form>
        </div>
    </div>
</header>
<hr>
<main>
    <form id="addProduct" name="addProduct" method="post" action="/saveproduct">
        <div class="inputsContainer">
            <div id="errDiv"></div>
            <div>
                <label for="sku" class="input">SKU</label>
                <input type="text" id="sku" name="sku" class="form-control">
            </div>

            <div>
                <label for="name" class="input">Name</label>
                <input type="text" id="name" name="name" class="form-control" >
            </div>

            <div>
                <label for="price" class="input">Price ($)</label>
                <input type="text" step=".01" id="price" name="price" class="form-control">
            </div>

            <div>
                <label for="productType">Type switcher</label>
                <select id="productType" name="productType" class="form-select" onchange="getInput()" required>
                    <option disabled selected value="">Choose product type</option>
                    <option value="getDisc">DVD-Disc</option>
                    <option value="getBook">Book</option>
                    <option value="getFurniture">Furniture</option>
                </select>
            </div>
            
            <!-- Div for loading type specification form -->
            <div id="productSpec"></div>
        </div>
    </form>
</main>