<?php
$productType = new app\ProductType();
?>

<header>
    <div>
        <h1>Product List</h1>

        <div>
            <form method="post" action="/addproduct">
                <input type="submit" value="ADD">
            </form>
            <form>
            <input type="submit" value="MASS DELETE" form="productCheckBox">
            </form>
        </div>
    </div>

</header>
<hr>
<main>
    
    <form id='productCheckBox' method='post' action="/delete" class="productFormContainer">
        <?php
        $dbConnection = new app\dataBase\Database();
        $dbItems = $dbConnection->getProducts();

        foreach ($dbItems as $i => $item) {
            $type = $dbItems[$i]['type'];
            echo "<div class='class-border'>
                  <input type='checkbox' name='checkBox$i' value='".$dbItems[$i]['SKU']."'>
                  <br>
                  <p class='class-productInfo'>".$dbItems[$i]['SKU']."</p>
                  <p class='class-productInfo'>".$dbItems[$i]['Name']."</p>
                  <p class='class-productInfo'>".$dbItems[$i]['Price']."$</p>
                  ".$productType->$type($dbItems[$i])."      
                  </div>";
        };
        ?>
    </form>
    
</main>