<?php

require_once __DIR__."/../vendor/autoload.php";

use app\Router;
use app\controller\PageController;

$router = new Router();

$router->get('/', [PageController::class, 'productList']);
$router->get('/addProduct', [PageController::class, 'addProduct']);


$router->post('/delete', [PageController::class, 'massDelete']);
$router->post('/addproduct', [PageController::class, 'addProduct']);

$router->post('/', [PageController::class, 'productList']);
$router->post('/saveproduct', [PageController::class, 'saveProduct']);



$router->resolve();