let button = document.getElementById('save');
let inputs = document.getElementsByTagName('input');
let errContainer = document.getElementById('errDiv');
let select = document.getElementById('productType');

let submitForm = document.getElementById('addProduct');

let price = document.getElementById('price');

// this js code first of all finds out if select is set, after that it checks if other inputs are filled with proper type of data

button.addEventListener('click',() => {
    if (select.value === '') {

        errContainer.innerText = "Please, submit required data";
        errContainer.className = "error";
        return false;

    } else if (select.value === 'getDisc'){
        let discSize = document.getElementById('size');

        for (let input of inputs) {
            if (input.value === '' || select.value === '') {
                errContainer.innerText = "Please, submit required data";
                errContainer.className = "error";
                return false;
            } else if (isNaN(price.value) || isNaN(discSize.value)) {
                errContainer.innerText = "Please, provide the data of indicated type";
                errContainer.className = "error";
                return false;
            }
        }
        submitForm.submit();

    } else if (select.value === 'getBook') {
        let bookWeight = document.getElementById('weight');

        for (let input of inputs) {
            if (input.value === '' || select.value === '') {
                errContainer.innerText = "Please, submit required data";
                errContainer.className = "error";
                return false;
            } else if (isNaN(price.value) || isNaN(bookWeight.value)) {
                errContainer.innerText = "Please, provide the data of indicated type";
                errContainer.className = "error";
                return false;
            }
        }
        submitForm.submit();

    } else if (select.value === 'getFurniture') {
        let height = document.getElementById('height');
        let width = document.getElementById('width');
        let length = document.getElementById('length');

        for (let input of inputs) {
            if (input.value === '' || select.value === '') {
                errContainer.innerText = "Please, submit required data";
                errContainer.className = "error";
                return false;
            } else if (isNaN(price.value) || isNaN(height.value) || isNaN(width.value) || isNaN(length.value)) {
                errContainer.innerText = "Please, provide the data of indicated type";
                errContainer.className = "error";
                return false;
            }
        }
        submitForm.submit();
    }

})