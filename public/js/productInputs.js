function getInput()
{
    var productId = document.getElementById("productType");
    var selectedValue = productId.options[productId.selectedIndex].value;
    var nClass = window[selectedValue]();
    var outPut = document.getElementById('productSpec');

    outPut.innerHTML =  nClass;
}

function getDisc(){
    return '<div><label for="size">Size (MB)</label> <input type="number" name="size" id="size" class="form-control"></div><div><p>"Please, provide size"</p></div>';
}

function getBook(){
    return '<div><label for="weight">Weight (KG)</label> <input type="text" name="weight" id="weight" class="form-control"></div><div><p>"Please, provide weight"</p></div>';
}

function getFurniture(){
    return '<div><label for="height">Height (CM)</label> <input type="number" name="height" id="height" class="form-control"></div><div><label for="width">Width (CM)</label> <input type="number" name="width" id="width" class="form-control"></div><div><label for="length">Length (CM)</label> <input type="number" name="length" id="length" class="form-control"></div><div><p>"Please, provide dimensions"</p></div>';
}