<?php


namespace app;


class ProductType
{

    public function getFurniture($dbItem)
    {
        return "<p class='class-productInfo'>Dimension: ". $dbItem['Height'] ."X". $dbItem['Width'] ."X". $dbItem['Length'] ."</p>";
    }

    public function getBook($dbItem)
    {
        return "<p class='class-productInfo'>Weight: ". $dbItem['Weight'] ." KG</p>";
    }

    public  function getDisc($dbItem)
    {
        return "<p class='class-productInfo'>Size: ". $dbItem['Size'] ." MB</p>";
    }
}