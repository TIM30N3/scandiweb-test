<?php

$servername = "localhost";
$username = "root";
$password = "";

try {
    $conn = new PDO("mysql:host=$servername", $username, $password);

    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "CREATE DATABASE pro";

    // use exec() because no results are returned
    $conn->exec($sql);
    $conn->exec("USE pro");
    echo "Database created successfully<br>";

    // sql to create table
    $sql = "CREATE TABLE products (
    type varchar(30) NOT NULL,
  SKU varchar(30) PRIMARY KEY,
  Name VARCHAR(30) NOT NULL,
  Price DECIMAL(10,2) NOT NULL,
  Size INT(50) NULL,
  Weight DECIMAL(10,2) NULL,
  Height INT(50) NULL,
  Width INT(50) NULL,
  Length INT(50) NULL 
  )";

    // use exec() because no results are returned
    $conn->exec($sql);
    echo "Table MyGuests created successfully";

} catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}

$conn = null;