<?php


namespace app\database;

use PDO;

class Database
{
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $dbName = "pro";
    protected $pdo;

//here I connect to database
    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=$this->servername; port=3306; dbname=$this->dbName", $this->username, $this->password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $this->pdo;
    }


//getting product details
    public function getProducts()
    {
        $statement = $this->pdo->prepare("SELECT * FROM products");
        $statement->execute();
        $products = $statement->fetchAll(PDO::FETCH_ASSOC);
        $this->products = $products;

        return $products;
    }

//this functions are for adding product in database by product type
    public function getDisc($type, $sku, $name, $price, $size, $weight, $height, $width, $length)
    {
        $statement = $this->pdo->prepare("INSERT INTO products (type, SKU, Name, Price, Size) VALUES(:type, :sku, :name, :price, :size)");

        $statement->bindValue(':type', $type);
        $statement->bindValue(':sku',$sku);
        $statement->bindValue(':name',$name);
        $statement->bindValue(':price',$price);
        $statement->bindValue(':size',$size);

        return $statement->execute();
    }

    public function getBook($type, $sku, $name, $price, $size, $weight, $height, $width, $length)
    {
        $statement = $this->pdo->prepare("INSERT INTO products (type, SKU, Name, Price, Weight) VALUES(:type, :sku, :name, :price , :weight)");

        $statement->bindValue(':type', $type);
        $statement->bindValue(':sku',$sku);
        $statement->bindValue(':name',$name);
        $statement->bindValue(':price',$price);
        $statement->bindValue(':weight',$weight);

        return $statement->execute();
    }

    public function getFurniture($type, $sku, $name, $price, $size, $weight, $height, $width, $length)
    {
        $statement = $this->pdo->prepare("INSERT INTO products (type, SKU, Name, Price, Height, Width, Length) VALUES (:type, :sku, :name, :price, :height, :width, :length)");

        $statement->bindValue(':type', $type);
        $statement->bindValue(':sku', $sku);
        $statement->bindValue(':name', $name);
        $statement->bindValue(':price', $price);
        $statement->bindValue(':height', $height);
        $statement->bindValue(':width', $width);
        $statement->bindValue(':length', $length);

        return $statement->execute();
    }

//delete products from database by sku
    public function deleteProducts($sku)
    {
        $statement = $this->pdo->prepare("DELETE FROM products WHERE SKU=:sku");
        $statement->bindValue(':sku',$sku);
        $statement->execute();
    }

}