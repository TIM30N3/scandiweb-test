<?php

namespace app\controller;

use app\database\Database;
use app\Router;

class PageController
{
    public function productList(Router $router)
    {
        return $router->renderView('productList');
    }

    public function addProduct(Router $router)
    {
        return $router->renderView('addproduct');
    }

    public function massDelete()
    {
        $dbConnection = new Database();
        $dbItems = $dbConnection->getProducts();

        $numOfProducts = count($dbItems);

        for ($n = 0; $n < $numOfProducts; $n++) {
            $productSKU = $_POST["checkBox$n" ?? null] ?? null;

            if (isset($productSKU)) {
                (new Database)->deleteProducts($productSKU);
            }
        }


        header("Location:/");
    }

    public function saveProduct()
    {
        $type = $_POST['productType'];

        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $size = $_POST['size'] ?? null;
        $weight = $_POST['weight'] ?? null;
        $height = $_POST['height'] ?? null;
        $width = $_POST['width'] ?? null;
        $length = $_POST['length'] ?? null;

        $addProduct = new \app\dataBase\Database();
        $addProduct->$type($type, $sku, $name, $price, $size, $weight, $height, $width, $length);

        header("Location:/");
    }


}